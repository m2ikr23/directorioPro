import { EmpresaServiceProvider } from './../providers/empresa-service/empresa-service';

import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'Firebase';

import { HomePage } from './../pages/home/home';
import { ListPage } from '../pages/list/list';


const config={

  apiKey: "AIzaSyCGfAU1zYNldGSuXWgquUu539y9XT-ardQ",
  authDomain: "directoriopro-a95a5.firebaseapp.com",
  databaseURL: "https://directoriopro-a95a5.firebaseio.com",
  projectId: "directoriopro-a95a5",
  storageBucket: "directoriopro-a95a5.appspot.com",
  messagingSenderId: "20738296286"

};


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  
  @ViewChild(Nav) nav: Nav;

  
  rootPage: any ;
  pages: Array<{title: string, component: any}>;

 

  constructor(public platform: Platform, public statusBar: StatusBar,
              public splashScreen: SplashScreen, public empresaServ: EmpresaServiceProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(config);
    let currenUser = this.empresaServ.getCurrentUser()
    if(currenUser!=null){
      console.log(currenUser);
      this.rootPage=HomePage;
    }else{
      console.log(currenUser);
      this.rootPage=LoginPage;
    }

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
