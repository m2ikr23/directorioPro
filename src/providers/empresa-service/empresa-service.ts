import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Empresa } from './../../model/empresa';
import {Observable} from 'rxjs/observable'
import * as firebase from 'Firebase';


export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
};

@Injectable()
export class EmpresaServiceProvider {

  private user: firebase.User;


  empresas: Observable<any[]>;
  listEmpresas:Empresa[]=[];

  ref :any;

  constructor(public http: HttpClient) {

   
  }


  agregarFirebase(uid,empresa:Empresa){
   let  idemp=Date.now();
  
   firebase.database().ref('users/'+uid+'/'+idemp).set(empresa);
  }

obtenerFirebase(){
  this.ref  = firebase.database().ref('users/'+this.getUidUser()+'/' );
  this.ref.on('value',resp=>{
    this.listEmpresas = snapshotToArray(resp);
    console.log(this.listEmpresas);

  })
  
  }

  agregarEmpresa(uid,nombre, telefono,correo,direccion){
   let empresa = new Empresa(nombre,telefono,correo,direccion);
   this.agregarFirebase(uid,empresa);
   this.listEmpresas.push(empresa);
  }

  obtenerEmpresas(){
    const empresas = new Promise((resolve,reject)=>{
      resolve(this.listEmpresas.slice());
      reject(new Error("No hay empresas Registradas"));
      });

      return empresas;
    }


    addUser(email:string, password:string){
      firebase.auth().createUserWithEmailAndPassword(email,password)
                        .catch(function(error){

                          console.log(error.code + " " + error.message);

                        })
    }

    loginUser(email:string, password:string){

    return firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
                    .then(()=>{
                      return firebase.auth().signInWithEmailAndPassword(email,password);
                   }).catch((error)=>{
                        console.log(error)
                    });
                  
    }

    singInResult(){
      firebase.auth().getRedirectResult().then((result)=>{
        if(result.credential){
         // let token = result.credential.providerId;
          let user = result.user;
          console.log(user);
        }
      })
    }

    getAuntenticated():boolean {
      return this.user !=null;
    }
    
    getEmail(){
      if(firebase.auth().currentUser){
         this.user=firebase.auth().currentUser;
         console.log("uid: "+this.user.uid);
          return this.user && this.user.email;
          
          }
    
    }

    getCurrentUser(){
      return firebase.auth().currentUser;
    }

    getUidUser(){
      if(this.getCurrentUser){
        return this.user.uid
      }
      
    }
}
