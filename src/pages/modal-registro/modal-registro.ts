import { EmpresaServiceProvider } from './../../providers/empresa-service/empresa-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-modal-registro',
  templateUrl: 'modal-registro.html',
})
export class ModalRegistroPage {

  private idUser:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl:ViewController, public empresaServ:EmpresaServiceProvider) {
      
          this.idUser=this.navParams.get("uid");
  }

dismiss(){
  this.viewCtrl.dismiss()
}


agregar(form:NgForm){
  
  let nombre = form.value.nombre;
  let telefono = form.value.telefono;
  let correo = form.value.correo;
  let direccion = form.value.direccion;

  console.log(nombre + telefono + correo + direccion);

  this.empresaServ.agregarEmpresa(this.idUser,nombre,telefono,correo,direccion);

  this.empresaServ.obtenerEmpresas().then(res=>{
    console.log(res);
  }).catch(error=>{
    console.log(error);
  })
  this.dismiss();
}


}
