import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalRegistroPage } from './modal-registro';

@NgModule({
  declarations: [
    ModalRegistroPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalRegistroPage),
  ],
})
export class ModalRegistroPageModule {}
