import { EmpresaServiceProvider } from './../../providers/empresa-service/empresa-service';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public empresaServ:EmpresaServiceProvider,
                public viewCtrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  addUser(form:NgForm){
      let email = form.value.email;
      let password = form.value.password;
      this.empresaServ.addUser(email,password);
      this.dismiss();
    }
    dismiss(){
      this.viewCtrl.dismiss()
    }
    
}
