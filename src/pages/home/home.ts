import { EmpresaServiceProvider } from './../../providers/empresa-service/empresa-service';
import { Component } from '@angular/core';
import { NavController,ModalController } from 'ionic-angular';

import { ModalRegistroPage } from './../modal-registro/modal-registro';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  empresas:any;
  private userId:string;

  constructor(public navCtrl: NavController, public modalCtrl:ModalController,
                public empresaServ: EmpresaServiceProvider) {
                
     let email= this.empresaServ.getEmail();
     this.userId=this.empresaServ.getUidUser();
     console.log(email);
  
  }

   ionViewDidLoad(){
    
  this.empresaServ.singInResult();
 
   this.empresaServ.obtenerFirebase();

  
   }
  registro(){
    let modal =  this.modalCtrl.create(ModalRegistroPage,{"uid":this.userId});
    modal.present();
  }

}
