import { ModalRegistroPage } from './../modal-registro/modal-registro';
import { HomePage } from './../home/home';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { EmpresaServiceProvider } from './../../providers/empresa-service/empresa-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public loadingCtrl:LoadingController, public modalCtrl:ModalController,
              public toastCtrl:ToastController,public empresaServ:EmpresaServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  
  loginUser(form:NgForm){
    let email = form.value.email;
    let pass = form.value.password;
    this.presentLoading()
    this.empresaServ.loginUser(email,pass).then(()=>{
   
      this.navCtrl.insert(0,HomePage);
      this.navCtrl.popToRoot();
    }
    
  ).catch(error=>{
    if(error.code=="auth/user-not-found" ||error.code=="auth/wrong-password" ){
      let mensaje='El email o contraseña no coinciden, intentalo de nuevo.'
      setTimeout(()=>
      this.presentToast(mensaje) ,5500
    )
  }
    if(error.code=="auth/invalid-email"){
      let mensaje='El email no es correcto.'
      setTimeout(()=>
      this.presentToast(mensaje) ,5500
    )
    }

    if(error.code=="auth/network-request-failed"){
      let mensaje='No hay conexión a internet'
      setTimeout(()=>
      this.presentToast(mensaje) ,5500
    )
    }
  
    console.log(error);
    
  });
    
  }

  presentLoading(){
      const loader = this.loadingCtrl.create({
        content: "Espere por favor...",
        duration: 5000,
        dismissOnPageChange: true,

      });
      loader.present();
    }

    presentModal(){
      const modal= this.modalCtrl.create(ModalRegistroPage);
      modal.present();
    }

    presentToast(mensaje) {
      const toast = this.toastCtrl.create({
        message: mensaje,
        position: "middle",
        duration: 3000,
        
      });
      toast.present();
    }
  }
  


