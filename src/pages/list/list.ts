
import { EmpresaServiceProvider } from './../../providers/empresa-service/empresa-service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public empresaServ:EmpresaServiceProvider) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.empresaServ.obtenerFirebase()
    
   this.empresaServ.obtenerEmpresas().then(info=>{
    
    this.items=info;
    console.log(this.items);

   })
   
 
  }
}
